import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { KEYS } from '../constant';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
	products: Array<{
		name: string|null, img: string|null, 
		price: string|null, description: string|null}> = [];

	itemNameController = new FormControl('', [Validators.required]);
	descriptionController = new FormControl('', [Validators.required]);
	imageUrlController = new FormControl('', [Validators.required]);
	priceController = new FormControl('', [Validators.required]);

	constructor() {
		const localItems = localStorage.getItem(KEYS.LAMP_ITEMS);
		this.products = localItems == undefined ? [] : JSON.parse(localItems);
	}

	get isValid() {
		return !(this.itemNameController.valid && this.itemNameController.value != ''
		&& this.descriptionController.valid && this.descriptionController.value != ''
		&& this.imageUrlController.valid && this.imageUrlController.value != ''
		&& this.priceController.valid && this.priceController.value != '');
	} 

	addItem() {
		this.products.push({
			description: this.descriptionController.value,
			img: this.imageUrlController.value,
			name: this.itemNameController.value,
			price: this.priceController.value
		});
		
		this.itemNameController.reset()
		this.descriptionController.reset()
		this.imageUrlController.reset()
		this.priceController.reset();

		localStorage.setItem(KEYS.LAMP_ITEMS, JSON.stringify(this.products));
	}

	generate() {
		this.itemNameController.setValue("lamp");
		this.descriptionController.setValue("lamp description");
		this.imageUrlController.setValue("https://media.istockphoto.com/id/1217867447/es/foto/l%C3%A1mparas-sobre-la-mesa.jpg?s=2048x2048&w=is&k=20&c=4uoINbcJTM8tE5kawB6iNNfxs9pdgaK2faJIexyVA8c=");
		this.priceController.setValue("12");
	}
}
