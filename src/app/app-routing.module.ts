import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularHelperComponent } from './angular-helper/angular-helper.component';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
	{ 
		path: 'helper',
		component: AngularHelperComponent,
		canActivate: [AuthGuard]
	},
	{ 
		path: 'home',
		component: HomeComponent,
		canActivate: [AuthGuard]
	},
	{ 
		path: 'login',
		component: LoginComponent
	},
	{ path: '**', component: LoginComponent },  // Wildcard route for a 404
];;

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
