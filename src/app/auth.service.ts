import { Injectable } from '@angular/core';
import { KEYS } from './constant';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	loggedIN = false;

	constructor() { 
		this.loggedIN = localStorage.getItem(KEYS.USER_IS_LOGGED_IN) == "1";
	}

	isLoggedIn() {
		return this.loggedIN;
	}

	login() {
		localStorage.setItem(KEYS.USER_IS_LOGGED_IN, "1");
		this.loggedIN = true;
	}
}
