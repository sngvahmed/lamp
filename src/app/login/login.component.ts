import { Component } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		const isSubmitted = form && form.submitted;
		return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
	}
}

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent {
	constructor(private router: Router, private auth: AuthService) { }

	emailFormControl = new FormControl('', [Validators.required, Validators.email]);
	passwordFormController = new FormControl('', [Validators.required]);

	matcher = new MyErrorStateMatcher();

	login() {
		this.auth.login()
		if (this.emailFormControl.value == "ahmednasser@abdo.com" && this.passwordFormController.value == "&213jkasd@asdhqwe*&^&^%@^&**@^") {
			this.router.navigate(["/home"]);
		}
	}

	get isValid() {
		return this.emailFormControl.valid && this.passwordFormController.valid;
	}
}
