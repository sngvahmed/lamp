import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularHelperComponent } from './angular-helper.component';

describe('AngularHelperComponent', () => {
  let component: AngularHelperComponent;
  let fixture: ComponentFixture<AngularHelperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngularHelperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AngularHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
