import { Component } from '@angular/core';

@Component({
  selector: 'app-angular-helper',
  templateUrl: './angular-helper.component.html',
  styleUrls: ['./angular-helper.component.css']
})
export class AngularHelperComponent {
  title = 'lamp';
}
